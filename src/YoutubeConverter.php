<?php

namespace Hermes\YoutubeConverter;

use VideoConverter;

class YoutubeConverter
{
    public function __construct()
    {

    }

    /**
     * Download video to storage
     * 
     * @param       String              The direct URL to the video
     * @param       String              The relative path within storage to store the downloaded file at
     * @param       String              The format to save the file in
     * @return      String              Relative path to the downloaded file
     */
    public function downloadVideo($video_url, $storage_path, $format)
    {
        // Download the video
        $raw_video = file_get_contents($video_url);

        // Generate a file name
        $file_name = $this->generateFileName($format);

        // Determine the final file path
        $file_location = $storage_path . "/" . $file_name;

        // Actually place the video in storage
        Storage::put($file_location, $raw_video);

        // Return the final destination
        return $file_location;
    }
    
    /**
     * Convert video to mp3
     * 
     * @param       String              The input video's file path
     * @param       String              The relative path within storage to store the converted video file at
     * @param       String              The desired file name, if null a filename will be generated
     * @return      String              Relative path to the converted file
     */
    public function convertVideoToMp3($file_path, $storage_path, $file_name = null)
    {
        return VideoConverter::convertVideo($file_path, $storage_path, "mp3", $file_name);
    }
    
    /**
     * Generate file name
     * 
     * @param       String              The desired format      (if left out, will only return a file name)
     * @param       String              The deisred name        (if left out, random string will be generated)
     * @return      String              Filename with or without an extension
     */
    private function generateFileName($format = null, $file_name = null)
    {
        // If no name was given
        if (is_null($file_name))
        {
            // Generate a new name
            $file_name = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()), 1);
        }

        // If no format was given
        if (is_null($format))
        {
            // Return only the file name
            return $file_name;
        }
        // If we did receive a format
        else
        {
            // Return file name + extension
            return $file_name.".".$format;
        }
    }

}