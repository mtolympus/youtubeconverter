<?php

namespace Hermes\YoutubeConverter\Providers;

use Illuminate\Support\ServiceProvider;
use Hermes\YoutubeConverter\YoutubeConverter;

class YoutubeConverterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('youtube-converter', function() {
            return new YoutubeConverter;
        });
    }
}