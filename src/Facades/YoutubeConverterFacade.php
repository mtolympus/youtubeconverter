<?php

namespace Hermes\YoutubeConverter\Facades;

use Illuminate\Support\Facades\Facade;

class YoutubeConverterFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'youtuber-converter';
    }
}
